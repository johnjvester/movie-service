# `movie-service`

> The `movie-service` is a [Spring Boot](<https://spring.io/projects/spring-boot>) RESTful API that allows consumers to 
> locate a list of appropriate movies based upon a provided mood. The service performs a semantic search on a collection 
> of movies by their plot descriptions, via the use of [MongoDB Atlas](https://www.mongodb.com/cloud/atlas).

## Getting Started

[This article](https://www.mongodb.com/developer/products/atlas/java-spring-boot-vector-search/) can be used to set up 
a MongoDB Atlas account, with the `sample_mflix` sample database and the `embedded_movies` collection.  

You'll also need an active [OpenAI API](https://openai.com/blog/openai-api) account to generate embeddings on the fly.

The following environmental variables are required to run the `movie-service`:

* `MONGO_USER` - the username for the MongoDB Atlas account
* `MONGO_PASSWORD` - the password for the MongoDB Atlas account
* `MONGO_HOST` - the hostname for the MongoDB Atlas account
* `MONGO_DB` - the database name for the MongoDB Atlas account
* `MONGO_APP_NAME` - the application name within MongoDB Atlas
* `OPENAI_KEY` - the API key for the OpenAI account
* `OPENAI_URL` - the URL for the OpenAI service (defaults to `https://api.openai.com`)

## Using the `movie-service`

With the `movie-service` started, you use the following cURL command to retrieve a list of movies based upon a mood:

```shell
curl -X 'GET' \
  'http://localhost:8585/movies?mood=american+romance' \
  -H 'accept: application/json'
```

For the `american romance` mood, the `movie-service` returns a `200 OK` response and a list of movies which are the best 
match for the provided mood.  

Here's a single example movie for brevity:

```json
[
  {
    "title": "Slow West",
    "year": 2015,
    "runtime": 84,
    "released": "2015-04-16T00:00:00.000+00:00",
    "poster": "https://m.media-amazon.com/images/M/MV5BNTYxNDA5ODk5NF5BMl5BanBnXkFtZTgwNzMwMzIwNTE@._V1_SY1000_SX677_AL_.jpg",
    "plot": "A young Scottish man travels across America in pursuit of the woman he loves, attracting the attention of an outlaw who is willing to serve as a guide.",
    "fullplot": "'Slow West' follows a 16-year-old boy on a journey across 19th Century frontier America in search of the woman he loves, while accompanied by mysterious traveler Silas.",
    "lastupdated": "2015-08-28 00:08:15.283000000",
    "type": "movie",
    "directors": [
      "John Maclean"
    ],
    "imdb": {
      "rating": 7.0,
      "votes": 11997,
      "id": 0
    },
    "cast": [
      "Kodi Smit-McPhee",
      "Michael Fassbender",
      "Ben Mendelsohn",
      "Aorere Paki"
    ],
    "countries": [
      "UK",
      "New Zealand"
    ],
    "genres": [
      "Action",
      "Thriller",
      "Western"
    ],
    "tomatoes": null,
    "num_mflix_comments": 0,
    "plot_embeddings": null,
    "id": {
      "timestamp": 1463423976,
      "date": "2016-05-16T18:39:36.000+00:00"
    }
  }
]
```

You can find the swagger docs for the `movie-service` here when the service is running:

http://localhost:8585/swagger-ui/index.html

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
